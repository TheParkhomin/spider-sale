from django.db import models

from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import UserManager as BaseUserManager


class UserManager(BaseUserManager):
    """Extended manager for User model."""

    use_in_migrations = False

    def make(self,):
        """Make new user object."""
        obj, created = self.get_or_create()
        assert obj.is_active

        return obj


class UserQuerySet(models.QuerySet):
    """Extended queryset for User model."""

    def active(self, switcher=True):
        """Filter only active users."""

        return self.filter(is_active=switcher)


class User(AbstractUser):
    """Base user model."""

    username = models.CharField(
        "username",
        max_length=255,
        null=True,
        blank=True,
        unique=True,
        default=None,
    )

    email = models.EmailField(
        "email address", blank=True, null=True, default=None
    )

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ["email"]

    objects = UserManager.from_queryset(UserQuerySet)()

    class Meta:
        """Meta class."""

        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

    def __str__(self):
        """String method."""

        return "%s:%s" % (self.email, self.get_short_name())


class Category(models.Model):
    title = models.CharField(max_length=255, verbose_name="Название")
    translit = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"


class Product(models.Model):
    title = models.CharField(max_length=255, verbose_name="Название")
    image = models.ImageField(verbose_name="Изображение", null=True, blank=True)
    file = models.FileField(verbose_name="Файл")
    category = models.ForeignKey("Category", on_delete=models.CASCADE, verbose_name="Категория")
    price = models.IntegerField(verbose_name="Цена")
    description = models.TextField(verbose_name="Описание")
    articul = models.CharField(max_length=100, verbose_name="Артикул", unique=True)

    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Товары"


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    products = models.ManyToManyField(Product)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"
