from django.shortcuts import render
from app.models import Product
from app.models import Category


def index(request):
    products = Product.objects.all()
    categories = Category.objects.all()
    for product in products:
        print(product.image.url)
    return render(request, "app/index.html", {"products": products, "categories": categories})


def detail_product(request, pk):
    product = Product.objects.get(id=pk)
    categories = Category.objects.all()

    return render(request, "app/product.html", {"product": product, "categories": categories})


def categories(request):
    pass

def list_product_by_category(request):
    pass
