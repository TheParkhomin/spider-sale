from django.contrib import admin
from app import models

# Register your models here.


@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
    """Product model admin settings."""


@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    """Category model admin settings."""
