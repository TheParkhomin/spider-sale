FROM python:3.8
ENV PYTHONUNBUFFERED 1
RUN apt-get update; apt-get --assume-yes install binutils libproj-dev gettext postgresql-client netcat htop lsof telnet
RUN mkdir /code
WORKDIR /code
ADD . /code/
RUN pip install --no-cache-dir -r /code/requirements.txt